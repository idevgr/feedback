@extends('admin.index')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <br>
                    <a class="btn btn-info btn-sm" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

@if(Auth::user()->hasRole('admin'))
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Answers CSV</div>
                <div class="card-body">
                    <a href="{{route('admin.export')}}" class="btn btn-sm btn-danger">EXPORT</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
