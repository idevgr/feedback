<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,600,700,800,900&amp;subset=greek">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>Lyrarakis Wines | Feedback Form - You'll help us to get better if you answer a few questions!.</title>
  </head>
  <body>
    @guest
      <form id="loginform" class="col-md-12" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
          @csrf
          <div class="form-group">
            <label for="email" class="white">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group">
            <label for="password" class="white">{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group mt-4">
            <button type="submit" class="next-btn btn btn-lg btn-primary btn-first">
                {{ __('Login') }}
            </button>
          </div>
      </form>
    @endguest
    @auth
      @if(Auth::user()->hasRole('device'))
      <div id="app">
        <app-component>
        </app-component>
      </div>
      <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
      @endif
      @if(Auth::user()->hasRole('admin'))
      @endif
    @endauth
  </body>
</html>