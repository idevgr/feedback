<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('application');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
	Route::get('/steps', 'ApplicationController@steps');
	Route::get('/questions', 'ApplicationController@questions');
	Route::post('/answers/add', 'ApplicationController@addAnswer');
	Route::post('/visitors/add', 'ApplicationController@addVisitor');
	Route::post('/visitorsub', 'ApplicationController@visitorsub');
	Route::get('/admin/export', 'AdminController@export')->name('admin.export');
});

Route::get('/home', 'HomeController@index')->name('home');
