<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Step;
use App\Question;
use App\Answer;
use App\Visitor;

class ApplicationController extends Controller
{
    public function steps() {
    	$steps = Step::with('questions')->get();
    	return $steps->toJson();
    }

    public function questions() {
    	$questions = Question::with('step')->get();
    	return $questions->toJson();
    }

    public function addAnswer(Request $request) {
        $answer = Answer::where('visitor_name', $request->visitor_name)->where('question_id', $request->question_id)->first();
        $visitor = Visitor::where('name', $request->visitor_name)->first();
/* enable this to replace the old answers with new
        if(empty($answer)) {
            $answer = new Answer;
            $answer->visitor_name = $request->visitor_name;
            $answer->visitor_email = $request->visitor_email;
        }*/
        $answer = new Answer;
        $answer->visitor_name = $request->visitor_name;
        if(empty($request->visitor_email)) {
            $answer->visitor_email = $visitor->email;
        }
        else {
            $answer->visitor_email = $request->visitor_email;
        }
    	$answer->question_id = $request->question_id;
    	$answer->value = $request->value;
    	$answer->save();
        if($answer->question_id == 1) {
            $visitor->country = $answer->value;
            $visitor->save();
        }
    }

    public function addVisitor(Request $request) {
        $userbyname = Visitor::where('name', $request->visitor_name)->first();
        $userbymail = Visitor::where('email', $request->visitor_email)->first();
        if(empty($userbyname) && empty($userbymail)) {
            $visitor = new Visitor;
            $visitor->name = $request->visitor_name;
            $visitor->email = $request->visitor_email;
            $visitor->save();
        }
        else if(!empty($userbyname) && empty($userbymail)) {
            $visitor = $userbyname;
            $visitor->email = $request->visitor_email;
            $visitor->save();
            $answers = Answer::where('visitor_name', $visitor->name)->get();
            foreach($answers as $answer) {
                $answer->visitor_email = $visitor->email;
                $answer->save();
            }
        }
        else if(empty($userbyname) && !empty($userbymail)) {
            $visitor = $userbymail;
            $visitor->name = $request->visitor_name;
            $visitor->save();
            $answers = Answer::where('visitor_email', $visitor->email)->get();
            foreach($answers as $answer) {
                $answer->visitor_name = $visitor->name;
                $answer->save();
            }
        }
    }
    public function visitorsub(Request $request) {
        $sendy_url = 'http://send.lyrarakis.com';
        $list = $request->list;
        $country = $request->user_country;
        $name = $request->name;
        $email = $request->email;
        $postdata = http_build_query(
            array(
            'name' => $name,
            'email' => $email,
            'list' => $list,
            'user_country' => $country,
            'boolean' => 'true'
            )
        );
        $opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
        $context  = stream_context_create($opts);
        $result = file_get_contents($sendy_url.'/subscribe', false, $context);
        return $result;
    }
}
