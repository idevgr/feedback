<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Visitor;
use App\Step;
use App\Question;
use App\Answer;

class AdminController extends Controller
{
    public function export() {
		$questions = Question::with('answers')->get();
		$answers = Answer::with('question')->get();
		foreach($answers as $answer) {
			$answer->question_title = Question::where('id', $answer->question_id)->pluck('title')->first();
			if($answer->value == 'lC9uZmehjwTEZs763hougLMQ') {
				$answer->value = 'Germany';
			}
			if($answer->value == 'G2MWXWoGHMs24ICYYD7YpQ') {
				$answer->value = 'United Kingdom';
			}
			if($answer->value == 'oi5QsbHFeJMtElWpBpENew') {
				$answer->value = 'United States';
			}
			if($answer->value == 'pQh2mWuoLwRQwu46O14NQQ') {
				$answer->value = 'Canada';
			}
			if($answer->value == 'ZDNTNd892PS0892uemmF8F717w') {
				$answer->value = 'Belgium';
			}
			if($answer->value == 'l763U3pMF8sA2vTx4ff9f8hQ') {
				$answer->value = 'Switzerland';
			}
			if($answer->value == 'Es7YKKrTcxBRuO763zvQyqVg') {
				$answer->value = 'Ireland';
			}
			if($answer->value == 'tE82zYbbd892Sy7QwxeW68GA') {
				$answer->value = 'Finland';
			}
			if($answer->value == 'RivKFSHbw6L9YH8ckTQyVw') {
				$answer->value = 'Norway';
			}
			if($answer->value == '5LS98cR892rCWdRwGOg763FLng') {
				$answer->value = 'Netherlands';
			}
			if($answer->value == 'CJomiMi892fQMsNePeookieg') {
				$answer->value = 'Australia';
			}
			if($answer->value == 'wcWIC8jxYsIPs6FYK1R2jQ') {
				$answer->value = 'France';
			}
			if($answer->value == '0LrzV4T9Ja6DC763KZOL5KZw') {
				$answer->value = 'Sweden';
			}
			if($answer->value == 'w5G2XcUZqQyr9M892s6S6BOA') {
				$answer->value = 'Other';
			}
		}
		$csvExporter = new \Laracsv\Export();
		$csvExporter->build($answers, ['visitor_name', 'visitor_email', 'question_title', 'value'])->download('lyrarakis_feedback_form_answers.csv');
    }
}
