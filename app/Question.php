<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function step() {
    	return $this->belongsTo('App\Step', 'step_id');
    }

    public function answers() {
    	return $this->hasMany('App\Answer', 'question_id');
    }
}
